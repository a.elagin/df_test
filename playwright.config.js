// @ts-check
const env = require('dotenv').config().parsed

/** @type {import('@playwright/test').PlaywrightTestConfig} */
const config = {
    use: {
        headless: false,
        viewport: { width: 1280, height: 720 },
        ignoreHTTPSErrors: true,
        //video: 'on-first-retry',
        timeout: 160000,
        workers: env.WORKERS_AMOUNT
    },
};

module.exports = config;