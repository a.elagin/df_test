const { test, expect } = require('@playwright/test');
const env = require('dotenv').config().parsed

// Run tests in this file with portrait-like viewport.
test.use({
    //viewport: { width: 600, height: 900 },
    headless: true,
    timeout: 160000,
    acceptDownloads:true
});

const pause = async(ms)=>{
    return new Promise((resolve, reject) => {
        setTimeout(()=>{
            resolve();
        }, ms);
    })
}

const loginUserScenario = (username, modelName) => {
    return test('basic test ' + username, async ({ page }) => {
        const USERNAME = username;
        const PASSWORD = "123456p";
        await page.goto('https://www.dirtyfans.com/login');
        await page.waitForTimeout(2000);
        await page.click("input[name='username']");
        await page.type("input[name='username']", USERNAME);
        await page.click("input[name='password']");
        await page.type("input[name='password']", PASSWORD);
        await page.click("form.form-login > button");
        await page.waitForTimeout(2000);
        await page.goto('https://www.dirtyfans.com/settings');
        await pause(2000);
        const locator = page.locator('.profile-avatar__name');
        await expect(locator).toContainText(USERNAME);
        //get chat page
        await page.goto('https://www.dirtyfans.com/chat');
        const element = await page.waitForSelector('.chat-model-content__name');
        //find chat with model name and selected
        //await pause(2000);
        const selectorModelDialog = `div.chat-model-content:has-text("${modelName}")`;
        await page.click(selectorModelDialog);
        await page.waitForSelector('.chat-messages-panel__bars');
        //find last video file and click on file
        await page.waitForSelector('.msg-file__media--video');
        // Click last video file
        await page.click('.msg-file--model >> nth=-1', {timeout: 10000});
        await pause(140000);
    });
}

test.describe.parallel('suite', () => {
    console.log('using '+env.WORKERS_AMOUNT+' workers');
    for (let i = 1; i <= +(env.WORKERS_AMOUNT || 10); i++) {
        loginUserScenario('test_user_'+i, 'Eva');
    }

});
// npx playwright test  playwright.spec.js --timeout 60000 --workers 15