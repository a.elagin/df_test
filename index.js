const { chromium, ios_saf } = require("playwright");

const USERNAME = "eva";
const PASSWORD = "123456p";

(async () => {
    const browser = await chromium.launch({
        headless:true,
        launchOptions:{
            slowMo: 200,
            viewport: { width: 1280, height: 720 },
        },
        ignoreHTTPSErrors: true,
    });
    const page = await browser.newPage();

    await page.goto("https://www.dirtyfans.com/login");
    await page.click("input[name='username']");
    await page.type("input[name='username']", USERNAME);
    await page.click("input[name='password']");
    await page.type("input[name='password']", PASSWORD);
    await page.click("form.form-login > button");
    //await page.waitForSelector("form#uploadPhoto", { visible: true });


    await browser.close();
})();